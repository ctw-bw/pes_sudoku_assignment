

class SudokuSolver(object):
    """
    Solve a simple sudoku for which no "if-then" logic steps are required.
    """

    def __init__(self, sudoku):
        """
        Initiate solver
        =INPUT=
            sudoku - list of lists of int
                Each sub-list corresponds with a row, each sub-list element
                with a column.
        """
        self.sudoku = sudoku
        self.is_solved = False
        return


    def solve(self):
        """
        Keep looping through the sudoku and try options until a solution
        is found. Bail after too many iterations (9*9 at most).
        """

        n_iterate = 0
        while not self.is_solved and n_iterate < 81:
            
            # Step through the sudoku
            for row_index in range(0, 9):
                for column_index in range(0, 9):
                    
                    # Skip if a number is already filled
                    if self.sudoku[row_index][column_index]:
                        continue

                    # A number is allowed if all conditions are met
                    possible_numbers = self.compute_possible_numbers(
                        row_index, column_index)
                    
                    # If there is a unique option, fill it in
                    if len(possible_numbers) == 1:
                        self.insert_number(
                            row_index, column_index, possible_numbers[0])

            self.check_is_solved()
            n_iterate += 1
            print(f'Iteration {n_iterate}')
            self.print_solution()

        return


    def insert_number(self, row_index, column_index, number):
        """
        Insert a number in the given row and column indices
        =INPUT=
            row_index - int on range(0, 9)
            column_index - int on range(0, 9)
            number - int on range(1, 10)
        """
        self.sudoku[row_index].pop(column_index)
        self.sudoku[row_index].insert(column_index, number)
        return


    def compute_column(self, column_index):
        """
        Get all column elements in a list
        =OUTPUT=
            list of int
        """
        return [row[column_index] for row in self.sudoku]


    def compute_sub_grid_index(self, index):
        """
        For either a row or column index, compute the first and last index
        of the sub-grid within which the input is located.
        =INPUT=
            index - int
        =OUTPUT=
            index_from, index_to - tuple of int
        """
        sub_index = index // 3
        index_from = sub_index * 3
        index_to = sub_index * 3 + 3
        return (index_from, index_to)


    def check_in_row(self, row_index, number):
        """
        Check if a number is already present in a row with given index
        =OUTPUT=
            boolean
        """
        return number in self.sudoku[row_index]


    def check_in_column(self, column_index, number):
        """
        Check if a number is already present in a column with given index
        =OUTPUT=
            boolean
        """
        return number in self.compute_column(column_index)


    def check_in_subgrid(self, row_index, column_index, number):
        """
        Check if the input number is in the subgrid within which the given
        row_index and column_index are located.
        =OUTPUT=
            boolean
        """
        row_from, row_to = self.compute_sub_grid_index(row_index)
        column_from, column_to = self.compute_sub_grid_index(column_index)
        
        # Comprehension to get a list of booleans,
        # with each indicating whether number is in sub-row, for sub_columns
        is_in_sub_rows = [
            number in self.sudoku[sub_row_index][column_from:column_to]
            for sub_row_index in range(row_from, row_to)]
        return any(is_in_sub_rows)


    def compute_possible_numbers(self, row_index, column_index):
        """
        Check for valid numbers on the given row and column indices. 
        =OUTPUT=
            possible_numbers - list of int
        """
        possible_numbers = []
        for number in range(1, 10):
            if (self.check_in_row(row_index, number)
                    or self.check_in_column(column_index, number) 
                    or self.check_in_subgrid(row_index, column_index, number)):
                continue
            possible_numbers.append(number)
        return possible_numbers


    def check_is_solved(self):
        """
        Sets self.is_solved to True if there are no remaining zeros
        """
        self.is_solved = all([all(row) for row in self.sudoku])
        return 


    def print_solution(self):
        """
        Print the sudoku row-by-row to the REPL
        """
        for row in self.sudoku:
            print(row)
        print('\n')
        return
